import math
import random

taille_des_cartons= 20
taille_des_boites= 100
nombre_de_cartons= 10
nombre_de_boites= None

nombre_de_boites_carton= taille_des_cartons//taille_des_boites
if nombre_de_boites_carton == 0:
    print('les boites sont trop imposantes')
else:
    nombre_de_cartons= math.ceil(nombre_de_boites / nombre_de_boites_carton)

print (nombre_de_cartons)

def combien_de_cartons_pour_n_boites(taille_des_cartons, taille_des_boites, nombre_de_boites):
    nombre_de_boites_carton = taille_des_cartons // taille_des_boites
    if nombre_de_boites_carton == 0:
        return 0
    else:
        nombre_de_cartons = math.ceil(nombre_de_boites / nombre_de_boites_carton)
        return nombre_de_cartons

# Exemple d'utilisation
X1 = combien_de_cartons_pour_n_boites(taille_des_cartons=20, taille_des_boites=10, nombre_de_boites=4)
print(X1,2)

X2 = combien_de_cartons_pour_n_boites(taille_des_cartons=200, taille_des_boites=20, nombre_de_boites=5)
print(X2,1)

X3 = combien_de_cartons_pour_n_boites(taille_des_cartons=50, taille_des_boites=25, nombre_de_boites=10)
print(X3,5)

#Les cartons fond entre TC et TC2. Les boites fond entre TB et TB2.
#Si j'ai N boites et X cartons, comment je peux ranger les boites ? 

def list_boites(taille_mini,taille_max,nb_boites):
    liste_boites = [random.randint(taille_mini, taille_max) for i in range(nb_boites)]    
    return liste_boites
def list_cartons(taille_mini,taille_max,nb_cartons):
    liste_cartons = [{'boites': [], 'taille': random.randint(taille_mini, taille_max)} for i in range(nb_cartons)]
    return liste_cartons

def ranger_boitesV2(liste_cartons,liste_boites):
    non_range = []
    for boite in liste_boites :
        for carton in liste_cartons:
            if sum(carton['boites']) + boite <= carton["taille"]:
                carton['boites'].append(boite)
                break
        else :
            non_range.append(boite)             
    return {"non_range":non_range,"lisliste_cartons":liste_cartons}

TC = 3
TC2 = 20
TB = 2
TB2 = 15
N = 10
X = 10

liste_boites = [10,5,5,15]
liste_cartons = [{'boites': [], 'taille': 10}, {'boites': [], 'taille': 15}]
rangement = ranger_boitesV2(liste_cartons,liste_boites)
print("le rangement:",rangement)

liste_boites = list_boites(TB,TB2,X)
liste_cartons = list_cartons(TC,TC2,X)
rangement = ranger_boitesV2(liste_cartons,liste_boites)
print("le rangement avec des random:",rangement)

resultat_somme_N = (5,20,150,15,30)
print("Somme des 5 premiers entiers:", resultat_somme_N)

liste_N_I = (3, 5,15,45,75,150,300)
print("Liste de 5 entiers égaux à 3:", liste_N_I)

liste_N_aleatoire = (4, 1, 10,9,8,2,3,5,7)
print("Liste de 4 entiers aléatoires entre 1 et 10:", liste_N_aleatoire)

liste_echange = [1, 2, 3, 4, 5,6,7,9]
(liste_echange, 1, 3,6,9)
print("Liste après échange de positions:", liste_echange)

tableau_min = [5, 2, 8, 1, 9,15,60,75,25,90,150]
index_min = (tableau_min)
print("Index du premier élément minimum:", index_min)

indices_min =(tableau_min)
print("Indices de tous les éléments minimum:", indices_min)

matrice_I = (4, 3, 2)
print("Matrice 3x2 contenant l'entier 4:", matrice_I)

matrice_aleatoire = (2, 3, 1, 10,15,75,25,90)
print("Matrice 2x3 d'entiers aléatoires entre 1 et 10:", matrice_aleatoire)

matrice1 = [[1, 4], [10, 50]]
matrice2 = [[55, 60], [75, 85]]
resultat_somme_matrices =(matrice1, matrice2)
print("Somme de deux matrices:", resultat_somme_matrices)

chaine_entier =("42","75","25","80","155")
print("Chaîne convertie en entier:", chaine_entier)

TC = 3
TC2 = 20
TB = 2
TB2 = 15
N = 10
X = 10

liste_des_cartons = [
    {"boites":[], "taille": 5},
    {"boites":[], "taille": 6},
    {"boites":[], "taille": 6},
    {"boites":[], "taille": 10}
]
liste_des_boites = [3, 3, 3, 3, 4, 3, 2]
print("le rangement sans random:",ranger_boitesV2(liste_des_cartons, liste_des_boites ))

liste_boites = list_boites(TB,TB2,X)
liste_cartons = list_cartons(TC,TC2,X)
rangement = ranger_boitesV2(liste_cartons,liste_boites)
print("le rangement avec des random:",rangement)

def trouver_indice_premier_element_minimum(tableau):
    if not tableau:  
        return None

    element_minimum = tableau[0]
    indice_minimum = 0

    for i in range(1, len(tableau)):
       
        if tableau[i] < element_minimum:
            element_minimum = tableau[i]
            indice_minimum = i

    return indice_minimum

tableau_entiers = [18, 55, 20, 100, 9, 15, 60, 75, 25, 90, 150]
print("Tableau d'entiers:", tableau_entiers)

indice_premier_minimum = trouver_indice_premier_element_minimum(tableau_entiers)
print("Indice du premier élément minimum", indice_premier_minimum)

def numeriser(chaine) -> int:
    
    codes_ascii = [str(ord(c)) for c in chaine]
   
    resultat_str = '0'.join(codes_ascii)
  
    return int(resultat_str)

def denumeriser(n) -> str:
    
    n_str = str(n)
    
    parties = [partie for partie in n_str.split('0') if partie]
    resultat_str = ''.join(chr(int(partie)) for partie in parties)
    return resultat_str

chaine_originale = "abc"
nombre_numerise = numeriser(chaine_originale)
chaine_recuperee = denumeriser(nombre_numerise)

print("Chaine originale:", chaine_originale)
print("Nombre numerisé:", nombre_numerise)
print("Chaine récupérée:", chaine_recuperee)

def numeriser(chaine) -> int:
 
    codes_ascii = [str(ord(c)) for c in chaine]
    
    resultat_str = ''.join(codes_ascii)
   
    return int(resultat_str)

def denumeriser(n) -> str:
    n_str = str(n)
    
    if n_str[0] == '1':
        taille_caractere = 3
    else:
        taille_caractere = 2

# Convertit le nombre (représenté en tant que chaîne de caractères) en segments de taille fixe
# pour faciliter la conversion ultérieure en caractères ASCII.
# Cela est particulièrement important pour gérer les codes ASCII qui peuvent avoir une ou deux positions dans la chaîne.
# Par exemple, '65' représente 'A' en ASCII et '97' représente 'a'.
# La taille du segment est déterminée en fonction du premier caractère du nombre (1 ou 2 chiffres).
# Cela garantit que chaque segment correspond à un code ASCII complet.

    segments = [n_str[i:i+taille_caractere] for i in range(0, len(n_str), taille_caractere)]

    resultat_str = ''.join(chr(int(segment)) for segment in segments)
    return resultat_str

chaine_originale = "abc"
nombre_numerise = numeriser(chaine_originale)
chaine_recuperee = denumeriser(nombre_numerise)

print("Chaine originale:", chaine_originale)
print("Nombre numerisé:", nombre_numerise)
print("Chaine récupérée:", chaine_recuperee)

n = 7
k = 2

nombre_trinques = math.factorial(n) // (math.factorial(k) * math.factorial(n - k))

print("Nombre total de trinques entre les sept personnes :", nombre_trinques)

n = 9
k = 3

nombre_trinques = math.factorial(n) // (math.factorial(k) * math.factorial(n - k))

print("Nombre total de trinques entre les neuf personnes :", nombre_trinques)

from itertools import permutations
import random

# Chiffres à inclure dans les permutations (de 5 à 10)
chiffres_inclus = list(range(5, 11))

# Mélanger les chiffres de manière aléatoire
random.shuffle(chiffres_inclus)

# Générer la liste avec les chiffres répétés
tailles_boites = chiffres_inclus * 1  # Répéter deux fois pour avoir suffisamment de chiffres
tailles_boites_separees = [tailles_boites[i::len(chiffres_inclus)] for i in range(len(chiffres_inclus))]

# Générer toutes les permutations possibles
permutations_possibles = list(permutations(tailles_boites_separees))

nombre_permutations = len(permutations_possibles)

print(f"Nombre total de permutations possibles : {nombre_permutations}")

print("Toutes les permutations possibles:")
for permutation in permutations_possibles:
    print(permutation)

chaine = ['a', 'b', 'c']

def generate_permutation():
    while True:
        permutation = random.sample(chaine, len(chaine))
        if permutation[0] != 'a':
            return permutation

# Nombre total de permutations à générer
nombre_permutations = 5

for _ in range(nombre_permutations):
    permutation = generate_permutation()
    print(''.join(permutation))


from collections import deque

# Créer une deque avec quelques éléments
my_deque = deque([1, 2, 3, 2, 4, 2, 5])

# Utiliser la méthode count pour compter le nombre d'occurrences de la valeur 2
count_of_2 = my_deque.count(2)

# Afficher le résultat
print("Le nombre d'occurrences de la valeur 2 dans la deque est :", count_of_2)













